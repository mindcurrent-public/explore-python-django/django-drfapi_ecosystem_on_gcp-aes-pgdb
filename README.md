# django-drfapi_ecosystem_on_gcp-aes-pgdb

Deploy a basic (but extensible) Django + DRFAPI (Django Rest Framework API) configuration from a (default SQLite-based) localhost instance onto a secure VPC configuration using a PostgreSQL database running in an AES instance on GCP.

---

## Applicable Prior Art:
* search (prior projects): [jupyter](https://gitlab.com/search?group_id=7130843&scope=projects&search=jupyter) yields:
  * issue (mc-backend-prototypes): [Install Anaconda and Jupyter Labs in a Mac OS Environment](https://gitlab.com/mc-dev-team/build/back-end/mc-backend-prototypes/jupyter-based-data-science-tools/reports_users-teams_beta-env_v01/-/issues/1)
  * issue (mc_ai_ecosystem_drfapis_v1): [Clone, configure, and test the skeleton DRF project scaffold (using SQLite on localhost)](https://gitlab.com/mc-dev-team/build/back-end/dev_n_test/mc_ai_ecosystem_drfapis_v1/-/issues/2)
  * wiki (mc_ai_ecosystem_drfapis_v1): [Deployment Steps (Summary History)](https://gitlab.com/mc-dev-team/build/back-end/dev_n_test/mc_ai_ecosystem_drfapis_v1/-/wikis/Deployment-Steps-(Summary-History))
  * issue (scaffold_sql-to-py-to-pg_v01): [Clone, configure, and test the SQL-to-Python-to-PostgreSQL scaffolding](https://gitlab.com/mc-dev-team/build/back-end/mc-backend-prototypes/database-schemas/scaffold_sql-to-py-to-pg_v01/-/issues/3)
  * issue (etl-pgsql-to-pddf-in-jupyter-v01): [Identify Approaches for Implementation](https://gitlab.com/mc-dev-team/build/back-end/dev_n_test/etl-pgsql-to-pddf-in-jupyter-v01/-/issues/1)

  * issues ([sys-config_PC_Linux-Mint_c2022](https://gitlab.com/dpcunningham/sys-config_pc_linux-mint_c2022)):
    * [Configure: Juypter Notebooks to use a Python Virtual Environment](https://gitlab.com/dpcunningham/sys-config_pc_linux-mint_c2022/-/issues/44)
    * [Install: Jupyter Notebooks (using CLI+pip not GUI+Conda)](https://gitlab.com/dpcunningham/sys-config_pc_linux-mint_c2022/-/issues/43)
    * [Setup: Python Virtual Environments (using requirements.txt)](https://gitlab.com/dpcunningham/sys-config_pc_linux-mint_c2022/-/issues/42)
